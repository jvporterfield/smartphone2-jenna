﻿using Smartphone.Models;
using System;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
      //this is where your program starts
      //Entry Point

      var sc = new SmartphoneClass();

      sc.Make = "LG";
      sc.Model = "L38c";
      sc.FrontCamMegPix = (decimal)1.3;
      sc.RearCamMegPix = (decimal)8;
      sc.ScreenSizeIn = 5;
      sc.ScreenHeight = 1920;
      sc.ScreenWidth = 1080;
      
      //write the phone specs to the console
      Console.WriteLine("***My Smartphone Specs***");
      Console.WriteLine("\r\nMake: {0}, Model: {1}", sc.Make, sc.Model);
      Console.WriteLine("Screen Size: {0} inches({1} mm)", sc.ScreenSizeIn, sc.ScreenSizeMm);
      Console.WriteLine("Screen Resolution: {0} x {1}", sc.ScreenHeight, sc.ScreenWidth);
      Console.WriteLine("\r\n\r\n***Camera Specs***");
      Console.WriteLine("\r\nRear Camera: {0} MP, Front Camera: {1} MP", sc.RearCamMegPix, sc.FrontCamMegPix);

      Console.WriteLine("\r\n\r\nPress the AnyKey");
      Console.ReadKey();
    }
  }
}
