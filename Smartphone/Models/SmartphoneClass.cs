﻿
namespace Smartphone.Models
{
    // Our Sweet Smartphone Class
  
  public class SmartphoneClass
  {
    //This is where constructors and properties go.
    //make of smartphone
    public string Make { get; set; }

    //model of smartphone
    public string Model { get; set; }

    //screensize diagonal measurement in inches
    public decimal ScreenSizeIn { get; set; }
    
    /// <summary>
    /// Calculates screen size in millimeters
    /// </summary>
    public decimal ScreenSizeMm 
    {
      get 
      {
        return ScreenSizeIn * (decimal)25.4;
      }
    }

    //screenres 2254 x 960 eg. in pixels
    public int ScreenWidth { get; set; }
    public int ScreenHeight { get; set; }

    //camera megapixels front and rear
    public decimal FrontCamMegPix { get; set; }
    public decimal RearCamMegPix { get; set; }


  }
}
